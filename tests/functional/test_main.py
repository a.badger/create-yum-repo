from create_yum_repo import main


def test_main(capsys) -> bool:
    try:
        main.main(
            [
                "--base",
                "cs9",
                "-l",
                "./package_list/cs9-image-manifest.lock.json",
            ],
        )
    except SystemExit:
        pass
    assert "TBD" in capsys.readouterr().out
