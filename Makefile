.PHONY: depend lint-containerfile lint-python lint unit functional build login \
        publish

ifneq "$(ENV)" "CI"
  CONTAINER_ENGINE := podman
  IMAGE_NAME := pipelines-as-code
  IMAGE_TAG := latest
  RUN_IN_CONTAINER := ${CONTAINER_ENGINE} run --rm -i \
    -v ${PWD}/.hadolint.yaml:/root/.config/hadolint.yaml:Z \
    -v ${PWD}/Containerfile:/Containerfile:Z \
    ghcr.io/hadolint/hadolint:v2.10.0-debian
endif

ifeq "$(ENV)" "CI"
  CONTAINER_ENGINE := docker
  IMAGE_NAME := ${CI_REGISTRY_IMAGE}
  IMAGE_TAG := ${CI_COMMIT_REF_SLUG}
endif

depend:
	pip install --upgrade pip==22.2.2
	pip install pylint -r requirements.txt -r test_requirements.txt

lint-containerfile:
	${RUN_IN_CONTAINER} hadolint Containerfile

lint-python:
	flake8 .
	isort . --check --diff
	# find . -name '*.py' -exec pylint {} +

lint: lint-containerfile lint-python

unit:
	pytest --cov=create_yum_repo --cov-report term-missing --cov-fail-under=82 \
	-v tests/unit

build:
	${CONTAINER_ENGINE} build -f Containerfile -t ${IMAGE_NAME}:${IMAGE_TAG} .

functional:
	pytest -v tests/functional
ifeq "$(ENV)" "CI"
login:
	${CONTAINER_ENGINE} login -u ${CI_REGISTRY_USER} \
	-p ${CI_REGISTRY_PASSWORD} ${CI_REGISTRY}

publish:
	${CONTAINER_ENGINE} push ${IMAGE_NAME}:${IMAGE_TAG}
endif
